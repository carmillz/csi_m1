<!--Modal de confirmation -->
<div id="confirm" class="modal fade">
	<div class="modal-dialog  modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE876;</i>
				</div>				
			</div>
			<form method="POST" action="">
				<div class="modal-body">
					<p class="text-center"><?php echo $_SESSION['confirm'] ?></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success btn-block" data-dismiss="modal">OK</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal de refus -->
<div id="fail" class="modal fade">
	<div class="modal-dialog modal-fail">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>				
			</div>
			<div class="modal-body">
				 <ul class="text-justify"><?php echo $_SESSION['fail'] ?></ul>
			</div>
			<div class="modal-footer">
				<button class="btn btn-danger btn-block" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>


<?php

//on affiche le modal selon si on a un message de confirmation ou d'erreur
if(!empty($_SESSION['fail'])){
	echo '
	<script type="text/javascript">
		$(\'#fail\').modal(\'show\');
	</script>';
}
if(!empty($_SESSION['confirm'])){
	echo '
	<script type="text/javascript">
		$(\'#confirm\').modal(\'show\');
	</script>';
}

//on détruit les messages d'erreurs apres qu'on aie affiché les modals
if (!empty($_SESSION['confirm'])){
	unset($_SESSION['confirm']);
}
if (!empty($_SESSION['fail'])) {
	unset($_SESSION['fail']);
}

?>
