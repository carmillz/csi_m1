<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php require "head.php";

$_SESSION['confirm']="";
$_SESSION['fail']="";
if (isset($_POST['soumettre'])) {
	if (empty($_POST['nom']) || empty($_POST['email']) || empty($_POST['sujet']) || empty($_POST['message'])) {
		$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
	}

	if (empty($_POST['fail'])){
		$to = "admins@csi.fr";
		$subject = $_POST['sujet'];

		$message="<html><head></head><body>".$_POST['message']."<br>".$_POST['nom']."</body></html>";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= "From:" . $_POST['email'];

		mail($to, $subject, $message, $headers);

		$_SESSION['confirm']='Votre message a bien été envoyé et sera traité dans les meilleurs délais';
	}
}

?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php";
		require "modal.php";
	?>
		<div class="container site-content mt-1 pt-2 bg-white rounded">
			<div class="row">
				<div class="col-lg-10 offset-1 text-justify">
					<div class="row">
						<div class="col-lg-12 mb-2">
							<h2>Contact</h2>
						</div>
					</div>
					<form action="contact.php" method="POST">
						<div class="row ">
							<div class="form-group col-lg-6 required">
								<label  class="h4 control-label">Nom</label>
								<input type="text" class="form-control" name="nom" placeholder="Nom" required>
							</div>
							<div class="form-group col-lg-6 required">
								<label  class="h4 control-label">Email</label>
								<?php  
									if (isset($_SESSION['mail'])){
										echo '<input type="email" class="form-control" name="email" value="'.$_SESSION['mail'].'" required>';
									}
									else {
										echo '<input type="email" class="form-control" name="email" placeholder="Email" required>';
									}
								?>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-12 required">
								<label for="sujet" class="h4 control-label">Sujet</label>
								<input type="text" class="form-control" name="sujet" placeholder="Sujet" required>
							</div>
						</div>
						<div class="form-group required">
							<label class="h4 control-label">Votre message</label>
							<textarea name="message" class="form-control" rows="5" placeholder="Ecrivez votre message" required></textarea>
						</div>
						<button type="submit" class="btn bouton float-right" name="soumettre">Soumettre</button>
					</form>
				</div>
				<div class="col-lg-10 offset-1 text-danger">* Champs obligatoires</div>
			</div>
		</div>
		<!--footer-->
		<?php require "footer.php" ?>
</body>
</html>