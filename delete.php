<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php require "head.php";

	if (isset($_POST['delete'])) {
		require 'newpassword.php';
		//on utilise la fonction pour set un mot de passe aléatoire et empecher que quelq'un se reconnecte
		$mdp = generer_mot_de_passe(10);
		$sql="SELECT delete_account('".$mdp."',".$_SESSION['id_util'].")";
		$query=pg_query($sql);
		unset($_SESSION['id_util']);
		$_SESSION['delete']='Votre compte a bien été supprimé';
	}

	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util']) || isset($_POST['cancel'])) {
		header("Location: index.php");
		exit();
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>
	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Mon compte</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="list-group col-lg-3 text-center">
								<a href="account.php" class="list-group-item list-group-item-action active" >Mes informations</a>
								<a href="blacklist.php" class="list-group-item list-group-item-action">Ma liste noire</a>
							</div>
							<div class="col-lg-9 text-justify tab-content">
								<!-- Suppression du compte-->
								<div class="card card-outline-secondary">
									<div class="card-header">La suppression de votre compte est définitive. Voulez-vous continuer ?</div>
									<div class="card-body">
										<form method="POST" action="">
											<div class="d-flex flex-row-reverse">
												<button type="submit" class="btn bouton" name="cancel">Non</button>
												<button type="submit" class="btn btn btn-danger mr-2" name="delete">Oui</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>