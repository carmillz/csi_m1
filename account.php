<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>

	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Mon compte</h2>
					</div>
				</div>
				<?php 
					/*
					0  id
					1 mail
					2 mdp
					3 nom
					4 prenom
					5 filiere
					6 niveau
					7 admin
					8 compte actif
					9 description
					*/
					$sql= "SELECT * FROM utilisateur WHERE id_utilisateur=".$_SESSION['id_util'];
					$query=pg_query($sql);
					$row = pg_fetch_row($query);

					$_SESSION['confirm']="";
					$_SESSION['fail']="";

					if (isset($_POST['save'])){
						$_POST['description']=pg_escape_string($_POST['description']);
						$sql ="SELECT modify_description ('". $_POST['description']."',".$_SESSION['id_util'].") ";
						$query=pg_query($sql);
						$row[9]=$_POST['description'];
						$_SESSION['confirm']='Votre description a été enregistrée avec succès';
					}
					require 'modal.php';		
					
				?>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="list-group col-lg-3 text-center">
								<a href="account.php" class="list-group-item list-group-item-action active">Mes informations</a>
								<a href="blacklist.php" class="list-group-item list-group-item-action" >Ma liste noire</a>
							</div>
							<div class="col-lg-9 text-justify tab-content">
								<div id="infos" class="card card-outline-secondary tab-pane active" >
								<!-- Consultation/gestion des informations-->
									<div class="card-header">Mes informations</div>
									<div class="card-body">
										<div class="card-body">
											<ul class="list-group list-group-flush">
												<li  class="list-group-item">
													<form action="account.php" method="POST">
														<div class="form-group row">
															<label for="nom" class="col-lg-4 col-form-label">Nom</label>
															<div class="col-lg-8">
																<input type="text" class="form-control" value="<?= stripslashes($row[3]) ?>" disabled>
															</div>
														</div>
														<div class="form-group row">
															<label for="prenom" class="col-lg-4 col-form-label">Prénom</label>
															<div class="col-lg-8">
																<input type="text" class="form-control" value="<?= stripslashes($row[4]) ?>" disabled>
															</div>
														</div>
														<div class="form-group row">
															<label for="level" class="col-lg-4 col-form-label">Niveau</label>
															<div class="col-lg-8">
																<input type="text" class="form-control" value="<?= stripslashes($row[6]) ?>"  disabled>
															</div>				
														</div>
														<div class="form-group row">
															<label for="password" class="col-lg-4 col-form-label">Filière</label>
															<div class="col-lg-8">
																<input type="text" class="form-control" value="<?= stripslashes($row[5]) ?>"  disabled>
															</div>
														</div>
														<div class="form-group row">
															<label for="email" class="col-lg-4 col-form-label">Email</label>
															<div class="col-lg-8">
																<input type="email" class="form-control" value="<?= stripslashes($row[1]) ?>" disabled>
															</div>
														</div>
														<div class="form-group row">
															<label for="email" class="col-lg-4 col-form-label">Description</label>
															<div class="col-lg-8">
																<input type="text" class="form-control" name="description" value="<?= stripslashes($row[9]) ?>">
															</div>
														</div>
														<div class="d-flex flex-row-reverse">
															<button type="submit" class="btn bouton" name="save">Enregistrer</button>
														</div>
													</form>			
												</li>
												<li  class="list-group-item"><a href="reset.php">Réinitialiser mon mot de passe</a></li>
												<li  class="list-group-item"><a href="delete.php">Supprimer mon compte</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>