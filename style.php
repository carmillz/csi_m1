<?php
if (!isset($_SESSION['id_util']) ){
	$_SESSION['filiere'] ='MIASHS';
}

if ($_SESSION['filiere'] == 'MIAGE') {	//+ajouter condition de connexion session + confiruration du bleu
echo '  
	.couleur {
		background-color: #388cc8 !important; /*bleu*/
		border-radius: 3px;			
	}
	

	#footer {
		background-color: #6abce2; /*bleu*/
		border-radius: 3px;			
	}

	.adfontcolor{
		color :#388cc8;
	}

	.list-group-item {
		border : 1px solid #388cc8;
	}

	.list-group-item.active {
		background-color: #388cc8;
		border-color: #388cc8;
	}

	a {
		color: #388cc8;
	}

	.barre {
		border-left: 1px solid #388cc8;
	}

	.bouton {
		color:  #388cc8;
		background-color: #ffffff;
		border-color: #388cc8;
	}';
}  elseif ($_SESSION['filiere'] == 'SCO') {	//+ajouter condition de connexion session +configuration du rouge
	echo '
	.couleur {
		background-color: #962223 !important; /*rouge*/
		border-radius: 3px;			
	}
	

	#footer {
		background-color: #da2a34; /*rouge*/
		border-radius: 3px;			
	}

	.adfontcolor{
		color :#962223;
	}

	.list-group-item {
		border : 1px solid #962223;
	}

	.list-group-item.active {
		background-color: #962223;
		border-color: #962223;
	}

	a {
		color: #962223;
	}

	.barre {
		border-left: 1px solid #962223;
	}
	.bouton {
		color:  #962223;
		background-color: #ffffff;
		border-color: #962223;
	}';
} elseif ($_SESSION['filiere'] == 'MIASHS') {	//+ajouter condition de connexion session +configuration du violet
	echo '
	.couleur {
		background-color: #53265f !important; /*violet*/
		border-radius: 3px;
	}
			
	.barre {
		border-left: 1px solid #53265f;
	}
	#footer {
		background-color: #8c4d93; /*violet*/
		border-radius: 3px;
	}

	.adfontcolor{
		color :#53265f;
	}

	.list-group-item {
		border : 1px solid #53265f;
	}

	.list-group-item.active {
		background-color: #53265f;
		border-color: #53265f;
	}

	a {
		color: #53265f;
	}

	.bouton {
		color:  #53265f;
		background-color: #ffffff;
		border-color: #53265f;
	}';
}
?>