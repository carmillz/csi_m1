<footer class="m-1">
	<div class="container couleur text-light rounded " >
		<div class="row m-1">
			<div class="col-lg-8 my-auto">
				<ul class="d-flex justify-content-between">
					<li class="nav-item"><a class="nav-link" href="cgu.php"> C.G.U.</a></li>
					<li class="nav-item"><a class="nav-link" href="recrutement.php"> Recrutement</a></li>
					<li class="nav-item"><a class="nav-link" href="contact.php"> Contact</a></li>
				</ul>
			</div>

			<div class="col-lg-3 offset-1">
				<div class="row pl-5">
					<div class="row w-100">
						<h5 class="mx-auto">Rejoignez-nous !</h5>
					</div>
					<div class="row w-100">
						<div class="mx-auto">
							<a href="https://www.facebook.com/groups/IDMC.nancy/?ref=bookmarks"><img class="img" src="img/facebook.png"></a>
							<a href="https://twitter.com/idmc_nancy"><img class="img float-right" src="img/twitter.png"></a>
							<a href="http://institut-sciences-digitales.fr/"><img class="img float-right align-middle" src="img/idmc.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer" class="container">
		<div class="row">
			<div class="text-center text-light m-2 p-1 col-lg-12">© 2018 Copyright BOUREKHSAS - CHIPOT - OULD - SUZANNE</div>
		</div>
	</div>
</footer>