<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
	$_SESSION['page'] ='ad';
	if (isset($_POST['cloture'])){
		$sql="SELECT cloture(".$_REQUEST['cloture'].")";
		$query=pg_query($sql);
		unset($_POST['cloture']);
	}

	if (isset($_POST['inscrire'])){
		$sql="SELECT inscription (".$_SESSION['id_util'].",".$_GET['annonce'].",'".pg_escape_string($_POST['comm'])."',".$_POST['optradio'].")";
		$query=pg_query($sql);
		unset($_POST['inscrire']);

		$sql = "SELECT titre_an FROM annonce WHERE id_annonce=".$_GET['annonce'];
		$query= pg_query($sql);
		$row = pg_fetch_row($query);


		$req = "SELECT mail FROM utilisateur WHERE id_utilisateur IN (SELECT id_util FROM annonce WHERE id_annonce=".$_GET['annonce'].")";
		$result= pg_query($req);
		$res = pg_fetch_row($result);
		$to = $res[0];
		$subject = 'Nouvelle demande d\'inscription';

		$message="<html><body>
		Cher utilisateur,<br>
		<br>
		Vous avez reçu une nouvelle demande d'inscription à l'annonce <i>". $row[0]."</i>.<br>
		Connectez-sur votre compte afin de prendre une décision ! <br>
		A bientôt sur S2S!<br>
		<br>
		Les administrateurs de S2S.</body></html>";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: admins@csi.fr';

		mail($to, $subject, $message, $headers);
	}

	if (isset($_POST['accepter'])){
		$sql = "SELECT accepter (".$_GET['annonce'].",".$_REQUEST['accepter'].")";
		$query= pg_query($sql);

		$sql = "SELECT titre_an FROM annonce WHERE id_annonce=".$_GET['annonce'];
		$query= pg_query($sql);
		$row = pg_fetch_row($query);


		$req = "SELECT mail FROM utilisateur WHERE id_utilisateur=".$_REQUEST['accepter'];
		$result= pg_query($req);
		$res = pg_fetch_row($result);
		$to = $res[0];
		$subject = 'Inscription acceptée';

		$message="<html><body>
		Cher utilisateur,<br>
		<br>
		Vous demande d'inscription à l'annonce <i>". $row[0]."</i> a été validée et vous êtes désormais inscrit(e).<br>
		A bientôt sur S2S!<br>
		<br>
		Les administrateurs de S2S.</body></html>";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: admins@csi.fr';

		mail($to, $subject, $message, $headers);
	}
	if (isset($_POST['refuser'])){
		$sql = "SELECT refuser (".$_GET['annonce'].",".$_REQUEST['refuser'].")";
		$query= pg_query($sql);

		$sql = "SELECT titre_an FROM annonce WHERE id_annonce=".$_GET['annonce'];
		$query= pg_query($sql);
		$row = pg_fetch_row($query);

		$req = "SELECT mail FROM utilisateur WHERE id_utilisateur=".$_REQUEST['refuser'];
		$result= pg_query($req);
		$res = pg_fetch_row($result);
		$to = $res[0];
		$subject = 'Inscription refusée';

		$message="<html><body>
		Cher utilisateur,<br>
		<br>
		Vous demande d'inscription à l'annonce <i>". $row[0]."</i> a été refusée.<br>
		A bientôt sur S2S!<br>
		<br>
		Les administrateurs de S2S.</body></html>";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: admins@csi.fr';

		mail($to, $subject, $message, $headers);
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>
	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<?php 
				$sql= "SELECT * from annonce
						WHERE  id_annonce =".$_GET['annonce'];
				$query=pg_query($sql);
				$_SESSION['requete']=$query;		
			?>
			<div class="col-lg-10 offset-1 ">
				<h2><?= $query[2] ?></h2>
			</div>
			<?php require 'anad.php'; ?> 
		</div>
	</div>
		
	<script>
		$(function () {
 			$('[data-toggle="tooltip"]').tooltip()
		})
	</script>
	<?php require "footer.php" ?>
</body>
</html>