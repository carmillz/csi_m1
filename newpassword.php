<?php
	/* mot de passe aleatoire
	 * @param $nb_caractere = longeur du mot de passe aléatoire voulu
	 * @author PHPAscal.com
	 * @since Thu Apr 22 13:51:04 EDT 2014
	 * @return string
	 */
	//on utilise la fonction pour set un mot de passe aléatoire
	function generer_mot_de_passe($nb_caractere){
		$mot_de_passe = "";
		$chaine = "abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
		$longueur_chaine = strlen($chaine);
	   
		for($i = 1; $i <= $nb_caractere; $i++){
			$place_aleatoire = mt_rand(0,($longueur_chaine-1));
			$mot_de_passe .= $chaine[$place_aleatoire];
		}
		return $mot_de_passe;
	}
?>