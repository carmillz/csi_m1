<!DOCTYPE html>
<html lang="fr">
<?php 
	require "head.php";
	$page ='index'; 
?>	

<body class="page bg-light">
	<?php
	require "header.php";
	?>

	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1 ">
				<h2> Les annonces de <?= $_GET['prenom'].' '.$_GET['nom']  ?></h2>
			</div>
			<?php 
				$sql= "select * from annonce WHERE id_util NOT IN 
			
			(select id_utilisateur from utilisateur, 
					(select id_util_1 from relation 
					WHERE relation.id_util_2=".$_SESSION['id_util']." 
					AND relation.statut_rel='banni') AS R1 
					WHERE utilisateur.id_utilisateur = R1.id_util_1)
					AND id_util = ".$_GET['util']."
					AND date_insc_max> CURRENT_TIMESTAMP
					AND (filiere_dest='".$_SESSION['filiere']."' OR filiere_dest='aucun')
					AND (niv_dest='".$_SESSION['niveau']."' OR niv_dest='tous')";
				$query=pg_query($sql);
				$_SESSION['requete']=$query;
				require 'anad.php';
			?>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>