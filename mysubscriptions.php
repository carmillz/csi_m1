<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
	$_SESSION['page'] ='mysubscription'; 
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>

	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2> Mes inscriptions</h2>
					</div>
				</div>
			</div>
			<?php 
				$sql= "SELECT * FROM annonce
						WHERE statut_an = 'en_cours'
						AND id_annonce IN (SELECT id_annonce FROM est_inscrit WHERE id_util=".$_SESSION['id_util'].")";
				$query=pg_query($sql);
				if (pg_num_rows($query) == 0 ){
					echo '<div class="alert alert-primary col-lg-10 offset-1 text-center mt-3" role="alert">Vous ne vous êtes inscrit(e) à aucune annonce pour le moment !</div>';
				}
				else {
					$_SESSION['requete']=$query;
					require 'anad.php';
				}
			?>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>