<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php"; 
		unset($_SESSION['fail']);
		unset($_SESSION['confirm']);

		if (isset($_POST['save_password'])) {
			$_POST['newpassword']=pg_escape_string($_POST['newpassword']);
			$_POST['repeatpassword']=pg_escape_string($_POST['repeatpassword']);
			$_SESSION['fail']="";
			$_SESSION['confirm']="";

			//on verifie si les champs sont vides
			if (empty($_POST['newpassword']) || empty($_POST['repeatpassword'])) {
				$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli, recommencez.</li>";
			}

			if ($_POST['newpassword'] != $_POST['repeatpassword']) {
				$_SESSION['fail']= "<li>Les mots de passe sont différents, recommencez.</li>";
			}
			if(empty($_SESSION['fail'])){
				$sql="SELECT modify_password('".crypt($_POST['newpassword'],'md5')."',".$_SESSION['id_util'].")";
				$query=pg_query($sql);
				$_SESSION['confirm']='Votre nouveau mot de passe a été sauvegardé !';
			}
		}
		require 'modal.php';
	?>
	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Mon compte</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="list-group col-lg-3 text-center" role="tablist">
								<a href="account.php" class="list-group-item list-group-item-action active" >Mes informations</a>
								<a href="blacklist.php" class="list-group-item list-group-item-action">Ma liste noire</a>
							</div>
							<div class="col-lg-9 text-justify tab-content">
								<!-- Réinitialisation du mdp-->
								<div id="reset" class="card card-outline-secondary">
									<div class="card-header">Réinitialiser mon mot de passe </div>
										<div class="card-body">
											<form method="POST" action="reset.php">
												<div class="form-group row required">
													<label for="password" class="col-lg-4 col-form-label control-label">Nouveau mot de passe</label>
													<div class="col-lg-8">
														<input type="password" class="form-control"  name="newpassword" placeholder="Mot de passe">
													</div>
												</div>
												<div class="form-group row required">
													<label for="password" class="col-lg-4 col-form-label control-label">Répétez le mot de passe</label>
													<div class="col-lg-8">
														<input type="password" class="form-control" name="repeatpassword" placeholder="Mot de passe">
													</div>
												</div>
												<div class="d-flex flex-row-reverse">
													<button type="submit" class="btn bouton" name="save_password">Enregistrer</button>
												</div>
											</form>
										</div>
									</div>
								<div class="text-danger">* Champs obligatoires</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>