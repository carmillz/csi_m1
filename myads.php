<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
	$_SESSION['page'] ='myads'; 
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>

	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-4 mb-2">
						<h2>Mes publications</h2>
					</div>

					<ul class="nav nav-tabs col-lg-4 offset-4" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="cours-tab" data-toggle="tab" href="#cours" role="tab" aria-controls="cours" aria-selected="true">En cours</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="cancel-tab" data-toggle="tab" href="#cancel" role="tab" aria-controls="cancel" aria-selected="false">Annulée</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="archive-tab" data-toggle="tab" href="#archive" role="tab" aria-controls="archive" aria-selected="false">Archivée</a>
						</li>
					</ul>
				</div>
				<div class="row tab-content" >
					<div class="tab-pane fade show active col-lg-12" id="cours" role="tabpanel" aria-labelledby="cours-tab">
						<div class="row">
						<?php 
							$sql= "SELECT * FROM annonce WHERE statut_an='en_cours' AND id_util=". $_SESSION['id_util'];
							$query=pg_query($sql);
							if (pg_num_rows($query) == 0 ){
								echo '<div class=" alert alert-primary col-lg-12 text-center mt-3" role="alert">Vous n\'avez pas publié d\'annonce pour le moment </div>';
							}
							else {
								$chaine ="";
								while ($row = pg_fetch_row($query)) {
									$sql2 = "SELECT id_annonce,titre_an FROM annonce WHERE statut_an='en_cours' AND id_util=". $_SESSION['id_util']. " AND date_insc_max < CURRENT_TIMESTAMP AND CURRENT_TIMESTAMP < date_fin_evt" ;
									$requete= pg_query($sql2);
									
									while ($ligne=pg_fetch_row($requete)){
										$chaine= $chaine.'<li><a href="ad.php?annonce='.$row[0].'">'. $row[3].'</a></li>';
									}
								}
								if ($chaine != ""){
									echo'<div class="col-lg-12 mt-1 mb-1 alert alert-warning pb-0" role="alert"> Les utilisateurs ne peuvent plus se connecter aux annonces suivantes
									<ul>'.$chaine.'</ul></div>';
								}
								$query=pg_query($sql);
				
								$_SESSION['requete']=$query;
								require 'anad.php';
							}
						?>
						</div>
					</div>

					<div class="tab-pane fade col-lg-12" id="cancel" role="tabpanel" aria-labelledby="cancel-tab">
						<div class="row">
						<?php 
							$sql= "SELECT * FROM annonce WHERE statut_an='annule' AND id_util=". $_SESSION['id_util'];
							$query=pg_query($sql);
							if (pg_num_rows($query) == 0 ){
								echo '<div class="alert alert-primary  col-lg-12 text-center mt-3 " role="alert">Vous n\'avez pas clôturé d\'annonce pour le moment.</div>';
							}
							else {
								$_SESSION['requete']=$query;
								require 'anad.php';
							}
						?>
						</div>
					</div>

					<div class="tab-pane fade col-lg-12" id="archive" role="tabpanel" aria-labelledby="archive-tab">
						<div class="row">
						<?php 
							$sql= "SELECT * FROM annonce WHERE statut_an='archive' AND id_util=". $_SESSION['id_util'];
							$query=pg_query($sql);
							if (pg_num_rows($query) == 0 ){
								echo '<div class="alert alert-primary col-lg-12 text-center mt-3 " role="alert">Aucune annonce archivée pour le moment.</div>';
							}
							else {
								$_SESSION['requete']=$query;
								require 'anad.php';
							}
						?>
						</div>

					</div>
				</div>
			</div>	
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>