<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}

	$sql="SELECT statut_rel FROM relation 
				WHERE id_util_1=". $_SESSION['id_util']." 
				AND id_util_2 = ".$_GET['util'];
				$query=pg_query($sql);
				$row = pg_fetch_row($query);

	
	if (isset($_POST['bloquer'])) {
		if (pg_num_rows($query) == 0) {
			$sql="SELECT utilA_banni_utilB(".$_SESSION['id_util'].",".$_GET['util'].")";
			$query=pg_query($sql);
		}
		if (pg_num_rows($query) == 1) {
			$sql="SELECT utilA_banni_utilB_update(".$_SESSION['id_util'].",".$_GET['util'].")";
			$query=pg_query($sql);
		}
	}

	if (isset($_POST['debloquer'])) {
		$sql="SELECT utilA_debloque_utilB(".$_SESSION['id_util'].",".$_GET['util'].")";
		$query=pg_query($sql);
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>
		<div class="container site-content mt-1 pt-2 bg-white rounded" >
			<div class="row">
				<div class="col-lg-10 offset-1">
					<form class="row" action="<?php echo 'profil.php?util='.$_GET['util'] ?>" method="POST">
					<?php 
						$sql= "SELECT nom,prenom,description,niveau,filiere,administrateur,date_insc FROM Utilisateur WHERE id_utilisateur=".$_GET['util'];
						$query=pg_query($sql);
						$row = pg_fetch_row($query);
						if ($row[2]==''){
							$row[2]= 'Aucune description pour le moment';
						}
						echo '
						<div class="col-lg-12 mb-2">
							<h2>'.$row[1].' '.$row[0].'</h2>
						</div>
						<div class="col-lg-8 mt-1 text-justify" >
							<h5>Description</h5>'
							.$row[2].
						'</div>
						<div class="col-lg-4 barre" id="profile">
							<h5>'.$row[3].' '.$row[4].'</h5>
							<a href="research.php?util='.$_GET['util'].'&nom='.$row[0].'&prenom='.$row[1].'" class="mt-2 mb-2"> Ses annonces</a>
							<div class="row  mt-2 mb-2">
								<div class="col-lg-7 adfontcolor"> inscrit depuis le</div>
								<div class="col-lg-5 text-right">'.date('d/m/Y', strtotime($row[6])).'</div>
							</div>
							

							<div class="row mt-2 mb-1">
								<div class="col-lg-4 adfontcolor">Statut :</div>';

							if ($row[5] =="t"){
								echo '<div class="col-lg-8 text-right"> Administrateur</div>';
							}
							else {
								echo '<div class="col-lg-8 text-right"> Utilisateur</div>';
							}
							echo '
						</div>';

						if ($_SESSION['id_util'] != $_GET['util']) {
							$sql="SELECT statut_rel FROM relation WHERE id_util_1=". $_SESSION['id_util']." AND id_util_2 = ".$_GET['util'];
							$query=pg_query($sql);
							$row = pg_fetch_row($query);

							if (pg_num_rows($query) == 0 ||  $row[0] == 'non_banni') {
								echo '<button type="submit" class="btn bouton mt-2" name="bloquer">Bloquer</button>';
							}
							else {
								echo '<button type="submit" class="btn bouton mt-2" name="debloquer">Débloquer</button>';
							}
							echo'</div>';
						}
					?>
					</form>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php require "footer.php" ?>
</body>
</html>