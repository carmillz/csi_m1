<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}

	$_SESSION['confirm']="";
	$_SESSION['fail']="";
	//cas ou on renomme une catégorie
	if (isset($_POST['rename'])){
		if (empty($_POST['oldname']) || empty($_POST['newname'])) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
		}
		else {
			//on vérifie que le nom saisit n'existe pas déjà dans la base
			$sql= "SELECT nom_categorie FROM categorie";
			$query=pg_query($sql);
			$boolean = true;

			while (($row = pg_fetch_row($query)) && $boolean == true){
				if($_POST['newname']==$row[0]){
					$_SESSION['fail'] = $_SESSION['fail'].'<li>Cette catégorie existe déjà.</li>';
					$boolean=false;
				}
			}

			if (empty($_SESSION['fail'])){
				$sql="SELECT rename_cat ('".$_POST['newname']."','".$_POST['oldname']."')";
				$query=pg_query($sql);
				$_SESSION['confirm']='La catégorie a été renommée avec succès';
			}
		}
	}
	//cas où on veut fusionner deux catégories

	if (isset($_POST['mix'])){
		if (empty($_POST['cat1']) || empty($_POST['cat2'])) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
		}

		if ($_POST['cat1'] == $_POST['cat2']) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Les deux catégories sont identiques.</li>";
		}
			
		/*transaction  :  
		on renomme la categorie 2 par la categorie 1 dans la table categorie
		on remplace l'id de la categorie 2 par la categorie 1 dans la table annonce
		on retire la ligne contenant le  nom + l'id de la categorie 2 de la table categorie */
		if (empty($_SESSION['fail'])){
			$sql ="BEGIN TRANSACTION;SELECT fusion_categorie2_dans_cat1('".$_POST['cat1']."','".$_POST['cat2']."');COMMIT";
			$query=pg_query($sql);
			$_SESSION['confirm']='Les catégories ont été fusionnées avec succès';
		}
		
	}
?>

<body class="page bg-light">
	<?php 
		require "header.php";
		require 'modal.php';	?>
	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Mode administrateur</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3">
						<ul class="list-group list-group-flush">
							<li class="list-group-item">Les catégories :</li>
							<?php 
								//on affiche toutes les categories
								$sql= "SELECT nom_categorie FROM categorie";
								$query=pg_query($sql);
								while ($row = pg_fetch_array($query)) {
									echo '<li class="list-group-item">'. $row[0].' </li>';
								}
							?>
						</ul>
					</div>
					<div class="col-lg-9 text-justify tab-content">
						<div class="card card-outline-secondary" role="tabpanel">
							<div class="card-header">Renommer une catégorie</div>
							<div class="card-body">
								<div class="card-body" role="tablist">
									<form method="POST" action="admin.php">
										<div class="form-group row required">
											<label class="col-lg-4 col-form-label control-label">Ancien nom</label>
											<div class="col-lg-8">
												<select class="col-lg-12 custom-select" name="oldname">
													<option value="">Catégorie à renommer</option>
												<?php 
													$sql= "SELECT * FROM categorie";
													$query=pg_query($sql);
													while ($row = pg_fetch_row($query)){
														echo '<option value="'. $row[1].'">'. $row[1].'</option>';
													}
												?>
												</select>
											</div>
										</div>
										<div class="form-group row required">
											<label for="newname" class="col-lg-4 col-form-label control-label">Nouveau nom</label>
											<div class="col-lg-8">
												<input type="text" class="form-control "  name="newname" placeholder="Nouveau nom">
											</div>
										</div>
										<div class="d-flex flex-row-reverse">
											<button type="submit" class="btn bouton" name="rename">Renommer</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="card card-outline-secondary mt-2" role="tabpanel">
							<div class="card-header">Fusionner deux catégories</div>
							<div class="card-body">
								<div class="card-body" role="tablist">
									<form method="POST" action="admin.php">
										<div class="form-group row required">
											<label for="oldname" class="col-lg-4 col-form-label control-label">Catégorie 1</label>
											<div class="col-lg-8">
												<select class="col-lg-12 custom-select" name="cat1">
													<option value="">Catégorie 1</option>
												<?php 
													$sql= "SELECT * FROM categorie";
													$query=pg_query($sql);
													while ($row = pg_fetch_row($query)){
														echo '<option value="'. $row[1].'">'. $row[1].'</option>';
													}
												?>
												</select>
											</div>
										</div>
										<div class="form-group row required">
											<label for="newname" class="col-lg-4 col-form-label control-label">Catégorie 2</label>
											<div class="col-lg-8">
												<select class="col-lg-12 custom-select" name="cat2">
													<option value="">Catégorie 2</option>
												<?php 
													$sql= "SELECT * FROM categorie";
													$query=pg_query($sql);
													while ($row = pg_fetch_row($query)){
														echo '<option value="'. $row[1].'">'. $row[1].'</option>';
													}
												?>
												</select>
											</div>
										</div>
										<div class="d-flex flex-row-reverse">
											<button type="submit" class="btn bouton" name="mix">Fusionner</button>
										</div>
									</form>
									<small> La fusion conservera le nom de la première catégorie entrée</small>
								</div>
							</div>
						</div>
						<div class="text-danger">* Champs obligatoires</div>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>