<header class="container text-light pt-1 ">
	<div class="row">
		<nav class="navbar col-lg-12 navbar-expand-lg rounded couleur">
			<img src="img/s2s.png" alt="s2s" width="140px">
			<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav d-flex justify-content-between w-100 m-4">
					<?php 
					if(!isset($_SESSION['id_util'])):  ?>
						<li class="nav-item"><a class="nav-link" href="index.php">S'identifier</a></li>
						<?php else:  ?>
	 					<li class="nav-item"><a class="nav-link" href="allads.php">Toutes les annonces</a></li>
						<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Les annonces</a>
							<div class="dropdown-menu"> <!--bloc deroulant -->
								<a class="dropdown-item" href="myads.php">Mes publications</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="mysubscriptions.php">Mes inscriptions</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="publishad.php">Publier une annonce</a></li>
						<?php 
							if ($_SESSION['admin']== 't'){
								echo '<li class="nav-item"><a class="nav-link" href="admin.php">Mode admin</a></li>';
							}
						 ?>
					<li class="nav-item"><a class="nav-link" href="account.php">Mon compte</a></li>
					<li class="nav-item"><a class="nav-link" href="disconnect.php"><i class="material-icons">power_settings_new</i></a></li>
					<?php endif ?>			 
				</ul>
			</div>
		</nav>
	</div>
</header>