﻿<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}

	if (isset($_POST['publier'])) {
		$_SESSION['fail']="";
		$_SESSION['confirm']="";
		$_POST['deb']= date('Y-m-d G:i', strtotime($_POST['deb']));
		$_POST['fin']= date('Y-m-d G:i', strtotime($_POST['fin']));
		//verification si les champs sont vides
		if (empty($_POST['deb']) || empty($_POST['fin']) ||  empty($_POST['titre']) || empty($_POST['description']) || empty($_POST['max']) ) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
		}
		
		//verification si la data de debut est apres la date actuelle
		if ($_POST['deb']< date('Y-m-d G:i')){
			$_SESSION['fail']= $_SESSION['fail']."<li>La date de début ne peut pas être antérieure à la date et l'heure actuelle.</li>";
		}

		//verification si la date de debut est bien avant la date de fin
		if ($_POST['deb'] > $_POST['fin']) {
			$_SESSION['fail']= $_SESSION['fail']."<li>La date de début ne peut pas être postérieure à la date de fin.</li>";
		}

		//verification de la bonne saisie du nbr de places
		if (isset($_POST['places'])<0){
			$_SESSION['fail']= $_SESSION['fail']."<li>Le nombre de places disponibles doit être positif.</li>";
		}
		//verification si la date d'inscription max est bien entre la date de debut et la date de fin
		if ($_POST['max'] < $_POST['deb']) {
			$_SESSION['confirm']="";

		}
		else {
			$_SESSION['fail']= $_SESSION['fail']."<li>La date d'inscription max ne peut pas être postérieure à la date de début.</li>";
		}
		if ($_POST['filiere'] !='aucun'){
			//verification de la classe visée
			if (($_POST['niveau'] =='L1' && $_POST['filiere']!='MIASHS')){
				$_SESSION['fail'] = $_SESSION['fail']. "<li>Si vous êtes en L1, vous ne pouvez qu'être qu'en MIASHS.</li>";
			}
			if (($_POST['filiere']=='MIASHS' && $_POST['niveau'] !='L1')){
				$_SESSION['fail'] = $_SESSION['fail']. "<li>Si vous êtes en MIASHS, vous ne pouvez qu'être qu'en L1.</li>";
			}
		}

		//si les cas précédents sont respectés on comfirme l'inscription
		if(empty($_SESSION['fail'])){
			if (!empty($_POST['newcat'])){
				$sql="SELECT categorie ('".$_POST['newcat']."')";
				$query=pg_query($sql);
				$val = pg_fetch_row($query);
				$sql="SELECT ajout_annonce(".$_SESSION['id_util'].",".$val[0].",'".pg_escape_string($_POST['titre'])."','".pg_escape_string($_POST['description'])."','".$_POST['deb']."','".$_POST['fin']."','".$_POST['max']."','".$_POST['niveau']."','". $_POST['filiere']."',". $_POST['places'].", '". $_POST['frequence']."', ". $_POST['repet'].", '". pg_escape_string($_POST['lieu'])."')";
				$query=pg_query($sql);
			}
			else {
				$sql="SELECT ajout_annonce(".$_SESSION['id_util'].",".$_REQUEST['cat'].",'".pg_escape_string($_POST['titre'])."','".pg_escape_string($_POST['description'])."','".$_POST['deb']."','".$_POST['fin']."','".$_POST['max']."','".$_POST['niveau']."','". $_POST['filiere']."',". $_POST['places'].", '". $_POST['frequence']."', ". $_POST['repet'].", '". pg_escape_string($_POST['lieu'])."')";
				$query=pg_query($sql);
				$_SESSION['confirm']='L\'annonce a bien été enregistrée.';
			}
		}
	}
require 'modal.php';
?>
<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>

	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<div class="row">
			<div class="col-lg-10 offset-1 ">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Publier une annonce</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-1 mt-2 mb-2">
					<form action="" method="POST">
						<div class="form-group row required">
							<div class="col-lg-4 addate rounded text-center p-1">
								<label class="adfontcolor w-100 control-label pt-1">Date de début</label>
								<input type="datetime-local" class="form-control w-100" placeholder="Entrez une date" name="deb" required>

								<label class="adfontcolor w-100 control-label pt-1">Date de fin</label>
								<input type="datetime-local" class="form-control w-100" placeholder="Entrez une date" name="fin" required>

								<label class="adfontcolor w-100 control-label pt-1">Date d'inscription max</label>
								<input type="datetime-local" class="form-control w-100" placeholder="Entrez une date" name="max" required>

								<label class="adfontcolor w-100 pt-1">Nombre de places à pourvoir</label>
								<input type="number" min="0" class="w-100" placeholder="Entrez le nombre de places" name="places">

								<label for="level" class="adfontcolor w-100 control-label pt-1">Niveau visé</label>
								<select class="col-lg-12 custom-select" name="niveau" required>
									<option value="tous">Tous les niveaux</option>
									<option value="L1">L1</option>
									<option value="L2">L2</option>
									<option value="L3">L3</option>
									<option value="M1">M1</option>
									<option value="M2">M2</option>

								</select>

								<label  class="adfontcolor w-100 control-label pt-1">Filière visée</label>
								<select class="col-lg-12 custom-select" name="filiere" required>
									<option value="aucun">Aucune</option>
									<option value="MIASHS">MIASHS</option>
									<option value="MIAGE">MIAGE</option>
									<option value="SCO">Sciences Cognitives</option>
								</select>

								<label for="level" class="adfontcolor w-100 control-label pt-1">Nombre de répétition de l'annonce</label>
								<select class="col-lg-12 custom-select" name="repet" required>
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>

								<label for="level" class="adfontcolor w-100 control-label pt-1">Fréquence de répétition de l'annonce</label>
								<select class="col-lg-12 custom-select" name="frequence" required>
									<option value="jamais">Jamais</option>
									<option value="jour">Quotidienne</option>
									<option value="mois">Mensuelle</option>
									<option value="annee">Annuelle</option>
								</select>
							</div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-12 ml-1">
										<div class="row adbackground rounded p-2">
											<div class="col-lg-12">
												<div class="row">
													<label class="col-lg-12 adfontcolor control-label">Titre de l'événement</label>
													<input type="text" class="form-control" name="titre" placeholder="Ajoutez un titre" required>
												</div>
												
												<div class="row">
													<label class="col-lg-12 adfontcolor control-label">Catégorie</label>
													<select class="col-lg-5 custom-select" name="cat">
														<option value="">Choisir une catégorie</option>
													<?php 
														$sql= "SELECT * FROM categorie";
														$query=pg_query($sql);
														while ($row = pg_fetch_row($query)){
															echo '<option value="'. $row[0].'">'. $row[1].'</option>';
														}
													?>
													</select>
													<div class="col-lg-1">OU</div>
													<input type="text" class="form-control col-lg-6" name="newcat" placeholder="Créer votre catégorie">
												</div>
												
												<div class="row">
													<label class="col-lg-12 adfontcolor">Lieu de l'événement</label>
													<input type="text" class="form-control" name="lieu" placeholder="Ajoutez un lieu">
												</div>
			
												<div class="row">
													<label class="col-lg-12 adfontcolor control-label">Description de l'événement</label>
													<textarea class="form-control" name="description" rows="12" placeholder="Ajoutez une description de l'événement"  required></textarea>
													<button type="submit" class="btn bouton col-lg-4 offset-4 mt-2" name="publier">Publier</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-10 offset-1 text-danger">* Champs obligatoires</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>