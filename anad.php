<?php
/*    colonnes de la table annonce  ==> requete avec $row
0 id de l'annonce
1 id de l'utilisateur qui possède l'annonce
2 id de la categorie
3 titre de l'annonce 
4 description de l'annonce
5 date de publication
6 date debut event
7 date fin event
8 date postuler max
9 statut de l'annonce
10 niveau
11 filiere
12 nombre de personnes max
13 frequence repetition
14 nb repetition
15 lieu


   colonnes de la table est_inscrit  ==> requete avec $ligne
0 id_util inscrit à l'annonce
1 id_annonce 
2 date_inscription à une annonce
3 statut inscription
4 commentaire annonce
5 notification annonce
*/

while ($row = pg_fetch_row($_SESSION['requete'])) {
	$sql = "SELECT * FROM est_inscrit WHERE id_util=".$_SESSION['id_util']." AND id_annonce=".$row[0];
	$requete= pg_query($sql);
	$ligne=pg_fetch_row($requete);
	if ($ligne[3]=="liste_attente"){
		$ligne[3]="En attente";
	}
	if ($ligne[3]=="validé"){
		$ligne[3]="Validé";
	}
	if ($ligne[3]=="refusé"){
		$ligne[3]="Refusé";
	}

	if ($row[9]=='en_cours'){
		$row[9]='En cours';
	}
	if ($row[9]=='annule'){
		$row[9]='Clôturée';
	}
	if ($row[9]=='archive'){
		$row[9]='Archivée';
	}
	if ($row[11]=='aucun') {
		$row[11]='Aucune';
	}
	if (empty($row[12])) {
		$row[12]='Non limité';
	}

	//on vérifie si l'utilisateur est le propriétaire de l'annonce $row
	if ($row[1]==$_SESSION['id_util']) {
		$propriétaire=1;

	}
	else {
		$propriétaire=0;
	}
	//on vérifie si l'utilisateur est inscrit à de l'annonce $row
	if ($ligne[0]==$_SESSION['id_util']){
		$inscrit= 1;
	}
	else {
		$inscrit= 0;
	}
	//on verifie le contenu du lieu pour l'affichage
	if ($row[15]==''){
		$row[15]='Non renseigné';
	}
	if (strlen($row[4])>=190 && $_SESSION['page']!="ad") {

		$comment=substr($row[4], 0, 190);
		$comment = $comment.'... &nbsp;';
	}
	else {
		$comment =$row[4].'<div class="col-lg-12">&nbsp; &nbsp;</div>';
	}
	if ($_SESSION['page'] =="ad"){
		echo '
		<div class="col-lg-10 offset-1">
			<h2>'.$row[3].'</h2>
		</div>';
	}
	//traitement de l'annonce
	echo '<!--annonce-->';
	
	//l'affichage diffère selon la page
	if($_SESSION['page']=="myads"){
		echo'
		<div class="col-lg-12 mt-1 mb-1">
			<div class="row">
				<div class="col-lg-4 addate rounded-left">
					<div class="row">
						<div class="col-lg-6 adfontcolor">Débute le </div>
						<div class="col-lg-6 text-right">'.date('d/m/y  à G:i', strtotime($row[6])).'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-6 adfontcolor">Fini le</div>
						<div class="col-lg-6 text-right">'.date('d/m/y  à G:i', strtotime($row[7])).'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-6 adfontcolor">Inscription limite</div>
						<div class="col-lg-6 text-right">'.date('d/m/y  à G:i', strtotime($row[8])).'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-8 adfontcolor">Nbr de places</div>
						<div class="col-lg-4 text-right">'.$row[12].'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-7 adfontcolor">Filière visée</div>
						<div class="col-lg-5 text-right">'.ucfirst($row[11]).'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-7 adfontcolor">Niveau visé</div>
						<div class="col-lg-5 text-right">'.$row[10].'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-7 adfontcolor">Répétitions</div>
						<div class="col-lg-5 text-right">'.$row[14].'</div>
					</div>
					<div class="row mb-1">
						<div class="col-lg-8 adfontcolor">Fréquence de répétition</div>
						<div class="col-lg-4 text-right">'.$row[13].'</div>
					</div>
				</div>
				<div class="col-lg-8">';
	}
	else {
		echo'
		<div class="col-lg-10 offset-1 mt-1 mb-1">
			<div class="row">
				<div class="col-lg-2 addate rounded-left">
					<div class="row text-center">
						<div class="adfontcolor w-100">Débute le </div>
						<div class="w-100 mb-1">'.date('d/m/y  à G:i', strtotime($row[6])).'</div>
						<div class="adfontcolor w-100">Fini le</div>
						<div class="w-100 mb-1">'.date('d/m/y  à G:i', strtotime($row[7])).'</div>
						<div class="adfontcolor w-100 ">Inscription limite</div>
						<div class="w-100 mb-1">'.date('d/m/y  à G:i', strtotime($row[8])).'</div>
						<div class="adfontcolor w-100">Nbr de places</div>
						<div class="w-100">'.$row[12].'</div>';

					if ($inscrit ==1 && $propriétaire==0){
					echo '
						<div class="adfontcolor w-100">';
						if($ligne[5]== 't'){ 
							echo " Notifié";
						}
						 else {
							echo "Non notifié";
						}

						echo'
						</div>';
					}
					 echo '
					</div>
				</div>
				<div class="col-lg-10">';
	}
		echo'
					<div class="row h-100">
						<div class="col-lg-12 ml-1">
							<div class="row adbackground rounded-right h-100">';
								if ($_SESSION['page'] != "ad" && $_SESSION['page'] != 'mysubscription' ){
									echo '<div class="col-lg-12 border-bottom"><a href="ad.php?annonce='.$row[0].'"><h5>'.$row[3].'</h5></a></div>';
								}
								$sql= "SELECT id_utilisateur,nom,prenom FROM utilisateur WHERE id_utilisateur=". $row[1];
								$requete=pg_query($sql);
								$id = pg_fetch_row($requete);
								if (($_SESSION['page'] == "index"  || $_SESSION['page'] == "ad" || $_SESSION['page'] =='mysubscription') && $propriétaire ==0){
									echo '<div class="col-lg-12 border-bottom">Par <a href="profil.php?util='.$id[0].'">'.$id[2].' '.$id[1].'</a></div>';
								}
								$sql= "SELECT nom_categorie FROM categorie WHERE id_categorie=". $row[2];
								$requete=pg_query($sql);
								$cat = pg_fetch_row($requete);
								echo '
								<div class="col-lg-3 adfontcolor">Catégorie</div>
								<div class="col-lg-9">'.$cat[0].'</div>
								<div class="col-lg-3 adfontcolor">Lieu</div>
								<div class="col-lg-9">'.$row[15].'</div>
							</div>
						</div>
						<div class="col-lg-12 text-justify adbackground ml-1 mt-1 mb-1 pt-2 rounded-right"><div class="h-100">'.$comment.'</div></div>';

						$sql= "SELECT COUNT(*) FROM est_inscrit WHERE id_annonce=".$row[0]." AND statut_insc='validé'";
								$requete=pg_query($sql);
								$inscrits = pg_fetch_row($requete);
						echo '
						<div class="col-lg-12 ml-1">
							<form action=""  method="POST" class="row rounded-right adbackground h-100">
								<div class="col-lg-2 my-auto text-right adfontcolor">Inscrits</div>
								<div class="col-lg-2 my-auto text-left">'.$inscrits[0].'</div>';
						if ($propriétaire==0 && $inscrit == 1 && $row[9]=='En cours'){
							echo '
								<div class="col-lg-2 my-auto text-right adfontcolor">Statut</div>
								<div class="col-lg-2 my-auto text-left">'.$ligne[3].'</div>';					
						}
						if ($propriétaire==1 && $_SESSION['page'] != 'ad' && $row[9] != 'Annulée'){
							$sql= "SELECT COUNT(*) FROM est_inscrit WHERE id_annonce=".$row[0]." AND statut_insc='liste_attente'";
							$requete=pg_query($sql);
							$demandes = pg_fetch_row($requete);
							echo '
								<div class="col-lg-2 my-auto text-right adfontcolor">Demandes</div>
								<div class="col-lg-2 my-auto text-left">'.$demandes[0].'</div>
							';
						}

						if ($_SESSION['page'] !='ad' && $_SESSION['page'] !='myads'){
							if ($inscrit ==0){
						echo '
								<button type="button" class="btn bouton col-lg-2 offset-6" onclick="window.location.href=\'ad.php?annonce='.$row[0].'\'">Consulter </button>';
							}
							else {
								echo '
								<button type="button" class="btn bouton col-lg-2 offset-2" onclick="window.location.href=\'ad.php?annonce='.$row[0].'\'">Consulter </button>';	
							}
						}
						if ($_SESSION['page'] =='myads'){
							if ( $row[9] != 'Annulée'){
								echo '
								<button type="button" class="btn bouton col-lg-2 offset-2" onclick="window.location.href=\'ad.php?annonce='.$row[0].'\'">Consulter </button>';
							}
							else {
								echo '
								<div class="col-lg-2 my-auto text-right adfontcolor">Statut</div>
								<div class="col-lg-2 my-auto text-left">Annulée</div>		
								<button type="button" class="btn bouton col-lg-2 offset-2" onclick="window.location.href=\'ad.php?annonce='.$row[0].'\'">Consulter </button>';
							}	
						}				
						if ($_SESSION['page'] =='ad'){
							if ($propriétaire == 1){
								echo '
								<div class="col-lg-2 my-auto text-right adfontcolor">Statut</div>
								<div class="col-lg-2 my-auto text-left border-right">'.$row[9].'</div>';
								if ($row[9]=='En cours'){
									echo '
									<button type="submit" class="btn bouton col-lg-2 offset-2" value="'.$row[0].'" name="cloture">Clôturer</button>';
								}
							} 
							if( $inscrit == 0 && $propriétaire==0){
								echo '
								<button class="btn bouton col-lg-2 offset-6" type="button" data-toggle="collapse" data-target="#inscrire" aria-expanded="false" aria-controls="inscrire" >S\'inscrire</button>';
							}
						}
						echo '
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>';

		//afffichage de la liste d'attente et des personnes inscrites
		if ($_SESSION['page']=='ad' && $propriétaire ==1 && $row[9] == 'En cours'){
			echo '
			<div class="col-lg-10 offset-1 mt-1 mb-1">
				<div class="row">
					<div class="col-lg-12">
						<div class="text-center m-2"><h4>Les inscrits</h4></div>
						<div class="row mt-1 mb-1">';
						$sql = "SELECT  id_util FROM est_inscrit WHERE  id_annonce=".$row[0]." AND statut_insc='validé'";
						$queries= pg_query($sql);

						if (pg_num_rows($queries) != 0) {
							while ($id = pg_fetch_row($queries)){
								$sql = "SELECT prenom, nom FROM utilisateur WHERE id_utilisateur=".$id[0];
								$request= pg_query($sql);
								$prenom_nom=pg_fetch_row($request);
							 		echo '<a class=" text-center col-lg-4 " href="profil.php?util='.$id[0].'">'.$prenom_nom[0].' '. $prenom_nom[1].'</a>';
							}
						}
						
						else {
							echo '
							<div class="col-lg-12 alert alert-info text-center mt-4" role="alert">Vous n\'avez accepté aucune demande</div>';
						}
						 echo
						'
						</div>
					</div>
				</div>';


				echo
				'<div class="row">
					<div class="col-lg-12">
						<div class="text-center"><h4>Les demandes en attente</h4></div>';
						$sql = "SELECT  id_util,commentaire FROM est_inscrit WHERE  id_annonce=".$row[0]." AND statut_insc='liste_attente' ORDER BY date_insc DESC";
						$recherche= pg_query($sql);

						if ($row[12]<=pg_num_rows($queries)) {
							echo '
							<div class="row">
								<div class="alert alert-danger text-center col-lg-12" role="alert">Vous avez atteint le nombre de personnes inscrites</div>
							</div>';
						}
						else {
							
							if (pg_num_rows($recherche) != 0) {
								echo '
									<form action="" method="POST">
										<div class="row mt-4 mb-1">';
								while ($id_commentaire=pg_fetch_row($recherche)){
								
									$sql = "SELECT prenom, nom FROM utilisateur WHERE id_utilisateur=".$id_commentaire[0];
									$query= pg_query($sql);
									
									
									while ($prenom_nom=pg_fetch_row($query)){
								 		echo '
									 		<a class=" text-center col-lg-4 " href="profile.php?util='.$id_commentaire[0].'">'.$prenom_nom[0].' '. $prenom_nom[1].'</a>
											<button type="submit" class="btn btn-outline-success col-lg-2 offset-1" value="'.$id_commentaire[0].'" name="accepter">Accepter</button>
											<button type="submit" class="btn btn-outline-danger ml-2 mr-2 col-lg-2" value="'.$id_commentaire[0].'" name="refuser">Refuser</button>';
										if (($id_commentaire[1]) != ''){
											echo'
											<button type="button" class="btn btn-outline-info col-lg-2" data-toggle="tooltip" data-placement="right" title="'.stripslashes($id_commentaire[1]).'">Commentaires</button>';
										}
									}
								}
								echo '</div>
								</form>';
							}		
							if (pg_num_rows($query) == 0) {
								echo '
								<div class="row mt-1 mb-1">
									<div class="alert alert-info text-center mt-4 col-lg-12" role="alert">Vous n\'avez pas de demandes en attente</div>
								</div>';
							}
						}
						
						echo '		
						</div>
					</div>';
				echo '
			</div>
		';
		}
		if ($inscrit ==0 && $propriétaire==0 && isset($_GET['annonce'])){
			echo 
			'<div class="col-lg-10 offset-1 mt-1 mb-1 collapse" id="inscrire">
				<div class="row">
					<div class="col-lg-12 mt-2">
						<div class="text-center m-2"><h4>Formulaire d\'inscription</h4></div>
						<form action="" method="POST">
							<div class="row">
								<div class="col-lg-4"> Voulez-vous être notifié ? </div>
								<label class="col-lg-1" > Oui </label><input type="radio" value="true" name="optradio" required>
								<label class="col-lg-1 offset-1" > Non </label><input type="radio" value="false" name="optradio">
							</div>
							<div class="col-lg-12"> N\'hésitez pas à fournir des précisions utiles pour l\'inscription ! </div>
							<textarea class="col-lg-12" name="comm"></textarea>
							<button type="submit" class="btn bouton mt-3" name="inscrire">Soumettre</button>
						</form>
					</div>
				</div>
			</div>';
		}
		if ($inscrit == 1 && $_SESSION['page']=='ad'){
			echo'
			<div class="col-lg-10 offset-1 alert alert-info text-center mt-2" role="alert">Vous êtes inscrit(e) à cette annonce</div>';
		}
	}
?>
