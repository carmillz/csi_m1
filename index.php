<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php
	require "head.php";
	$_SESSION['page'] ='index'; 

?>	

<body class="page bg-light">
	<?php
	//si on veut se rendresur la page de connexion/inscription alors que l'on est dejà connecté
	if (isset($_SESSION['id_util']) || !empty($_SESSION['id_util'])) {
		header('Location:allads.php');
	}
	//on affiche que le compte a bien été supprimé
	if (isset($_SESSION['delete'])){
		$_SESSION['confirm']=$_SESSION['delete'];
		unset($_SESSION['delete']);
	}
	//cas ou on n'a pas supprimé le compte
	else {
		$_SESSION['confirm']="";
	}
	$_SESSION['fail']="";
	//INSCRIPTION
	//Si on clique sur le bouton inscription et que
	if (isset($_POST['inscrire'])) {
		//pg_escape_string permet de mettre des antislashs aux simple quote, double quote, aux antislashs et NULL, evite les injections de SQL
		$_POST['nom']=pg_escape_string($_POST['nom']);
		$_POST['prenom']=pg_escape_string($_POST['prenom']);
		$_POST['mail']=pg_escape_string($_POST['mail']);
		$_POST['password']=pg_escape_string($_POST['password']);
		$_POST['password2']=pg_escape_string($_POST['password2']);

		//verification si les champs sont vides
		if (empty($_POST['nom']) || empty($_POST['prenom']) || empty($_POST['mail']) || empty($_POST['password']) || empty($_POST['password2'])) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
		}
		//verification si les mots de passe sont differents
		if ($_POST['password'] != $_POST['password2']) {
			$_SESSION['fail']= "<li>Les mots de passe sont différents.</li>";
		}

		//verification de la classe de l'éleve
		if (($_POST['niveau'] =='L1' && $_POST['filiere']!='MIASHS')){
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Si vous êtes en L1, vous ne pouvez qu'être qu'en MIASHS.</li>";
		}
		if (($_POST['filiere']=='MIASHS' && $_POST['niveau'] !='L1')){
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Si vous êtes en MIASHS, vous ne pouvez qu'être qu'en L1.</li>";
		}
		
		$sql= "SELECT mail FROM utilisateur";
		$query=pg_query($sql);
		if (pg_num_rows($query) !=0){
			$boolean = false;
			while (($row = pg_fetch_row($query)) && $boolean == false){
				if ($_POST['mail'] == $row[0] ){
					$_SESSION['fail'] = $_SESSION['fail']. "<li> L'adresse mail saisie existe déjà, veuillez en saisir une nouvelle.</li>";
					$boolean = true;
				}
			}
		}

		//si les cas précédents sont respectés on comfirme l'inscription
		if(empty($_SESSION['fail'])){
			$sql = "SELECT ajout_utilisateur('".$_POST['mail']."','".crypt($_POST['password'],'md5')."','".ucfirst($_POST['nom'])."','".ucfirst($_POST['prenom'])."','".$_POST['filiere']."','".$_POST['niveau']."')";
			$query=pg_query($sql);
			$_SESSION['confirm']='Vous êtes désormais inscrit(e), connectez-vous et commencer à explorer nos annonces !';
		}
	}

	//CONNEXION
	if (isset($_POST['connecter'])) {
		//pg_escape_string permet de mettre des antislashs aux simple quote, double quote, aux antislashs et NULL, evite les injections de SQL
		$_POST['pswd']=pg_escape_string($_POST['pswd']);
		$_POST['email']=pg_escape_string($_POST['email']);

		$_SESSION['fail']="";
		//verification si les champs sont vides
		if (empty($_POST['email']) || empty($_POST['pswd'])) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
		}

		$sql= "SELECT id_utilisateur, mdp, administrateur, filiere, niveau FROM utilisateur WHERE mail='". $_POST['email']."' AND actif=true";
		$query=pg_query($sql);

		//on vérifie que le mdp correspond bien au mail entré SI on trouve un resultat
		if (pg_num_rows($query) == 1){
			$row =pg_fetch_row($query);
			if (crypt($_POST['pswd'], $row[1]) == $row[1]){
				$_SESSION['id_util'] = $row[0];
				$_SESSION['admin'] = $row[2];
				$_SESSION['filiere'] = $row[3];
				$_SESSION['niveau'] = $row[4];
				$_SESSION['mail']=$_POST['email'];
				header('Location:allads.php');
			}
			else {
				//sinon on affiche une erreur
				$_SESSION['fail'] = $_SESSION['fail']. "<li>Identifiants invalides.</li>";
			}
		}
		else {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Identifiants invalides.</li>";
		}
	}

	//RESET DU MOT DE PASSE
	if (isset($_POST['envoi'])) {
		//on vérifie si le champs est vide ou non
		if (empty($_POST['resetmail'])) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>Le champs n'est pas rempli.</li>";
		}

		//on vérifie si le mail est dans la base
		$sql="SELECT id_utilisateur FROM utilisateur WHERE mail='".$_POST['resetmail']."'";
		$query=pg_query($sql);
		$row =pg_fetch_row($query);
		if (pg_num_rows($query)==0) {
			$_SESSION['fail'] = $_SESSION['fail']. "<li>L'adresse mail est invalide.</li>";
		}

		if (empty($_SESSION['fail'])){
			require 'newpassword.php';
			$mdp = generer_mot_de_passe(10);
			$to = $_POST['resetmail'];
			$subject = 'Réinitialisation du mot de passe';

			$message="<html><body>
			Cher utilisateur,<br>
			<br>
			suite à votre demande, voici votre nouveau mot de passe afin de connecter : ".$mdp.".<br>
			Attention : votre précédent mot de passe n'est désormais plus valide.<br>
			A bientôt sur S2S!<br>
			<br>
			Les administrateurs de S2S.</body></html>";

			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers .= "From:admins@csi.fr";

			mail($to, $subject, $message, $headers);

			$sql="SELECT modify_password('".crypt($mdp,'md5')."',".$row[0].")";
			$query=pg_query($sql);
			$_SESSION['confirm']='Consultez votre messagerie électronique afin d\'obtenir votre nouveau mot de passe';
		}
	}

	require 'modal.php';
	require "header.php"; ?>

	<div class="container site-content mt-1 pt-5 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1 mt-5">
				<div class="row">
					<div class="col-lg-6 mt-1 my-auto" >
						<h5 class="text-center mb-3">Se connecter</h5>
						<form action="" method="POST">
							<div class="form-group row required">
								<label for="email" class="col-lg-4 col-form-label control-label">Email</label>
								<div class="col-lg-8">
									<input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Email">
								</div>
							</div>
							<div class="form-group required row">
								<label for="password" class="col-lg-4 col-form-label control-label">Mot de passe</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" name="pswd" placeholder="Mot de passe">
									<div class="float-right align-middle"><small><a href="#" data-toggle="modal" data-target="#forgottenpassword">Mot de passe oublié</a></small></div>
								</div>
							</div>
							<button type="submit" class="btn bouton clearfix float-right" name="connecter">Se connecter</button>
						</form>
					</div>

					<div class="col-lg-6 barre">
						<h5 class="text-center">S'inscrire</h5>
						<form action="" method="POST">
							<div class="form-group row required">
								<label for="nom" class="col-lg-4 col-form-label control-label">Nom</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" placeholder="Votre nom" name="nom">
								</div>
							</div>
							<div class="form-group row required">
								<label for="prenom" class="col-lg-4 col-form-label control-label">Prénom</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" placeholder="Votre prénom" name="prenom">
								</div>
							</div>
							<div class="form-group row required">
								<label for="level" class="col-lg-4 col-form-label control-label">Niveau</label>	
								<div class="col-lg-8">
									<select class="col-lg-12 custom-select" name="niveau" >
										<option value="L1">L1</option>
										<option value="L2">L2</option>
										<option value="L3">L3</option>
										<option value="M1">M1</option>
										<option value="M2">M2</option>
									</select>
								</div>
							</div>
							<div class="form-group row required">
								<label for="password" class="col-lg-4 col-form-label control-label">Filière</label>
								<div class="col-lg-8">
									<select class="custom-select col-lg-12 " name="filiere" >
										<option value="MIASHS">MIASHS</option>
										<option value="MIAGE">MIAGE</option>
										<option value="SCO">Sciences Cognitives</option>
									</select>
								</div>
							</div>
							<div class="form-group row required">
								<label for="email" class="col-lg-4 col-form-label control-label">Email</label>
								<div class="col-lg-8">
									<input type="email" class="form-control" name="mail" placeholder="Email" >
								</div>
							</div>
							<div class="form-group row required">
								<label for="password" class="col-lg-4 col-form-label control-label">Mot de passe</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" name="password" placeholder="Mot de passe" >
								</div>
							</div>
							<div class="form-group row required">
								<div class="col-lg-8 offset-4">
									<input type="password" class="form-control control-label" name="password2" placeholder="Répétez le mot de passe" >
								</div>
							</div>
							<button type="submit" name="inscrire" class="btn bouton float-right">S'inscrire</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-10 offset-1 text-danger">* Champs obligatoires</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="forgottenpassword" tabindex="-1" role="dialog" aria-labelledby="forgottenpassword" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Mot de passe oublié ?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="" method="POST">
						<div class="form-group m-2 required">
							<label clas="control-label">Entrez votre email pour recevoir un nouveau mot de passe</label>
							<input type="mail" class="form-control mt-2 mb-2" name="resetmail">
							<button type="submit" class="btn bouton" name="envoi">Envoyer</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>