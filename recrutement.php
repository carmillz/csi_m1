<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
?>

<body class="page bg-light">
	<?php 
		require "header.php";
		$_SESSION['fail']="";
		$_SESSION['confirm']="";

		if (isset($_POST['soumettre'])) {
			if (empty($_POST['nom']) || empty($_POST['email'])  || empty($_POST['message'])) {
				$_SESSION['fail'] = $_SESSION['fail']. "<li>Au moins un des champs n'est pas rempli.</li>";
			}
			if (empty($_SESSION['fail'])){
				$to = "admins@csi.fr";
				$subject = "Recrutement administrateur";

				$message="<html><head></head><body>".$_POST['message']."<br>".$_POST['nom']."</body></html>";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
				$headers .= "From:" . $_POST['email'];

				mail($to, $subject, $message, $headers);

				$_SESSION['confirm']='Votre message bien été envoyé et sera traité dans les meilleurs délais';
			}
		}
		require "modal.php";
	?>
	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<form action="" method="POST" class="row">
			<div class="col-lg-10 offset-1 text-justify">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Recrutement administrateur</h2>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-lg-6 required">
						<label class="h4 control-label">Nom</label>
						<input type="text" class="form-control" name="nom" placeholder="Nom" required>
					</div>
					<div class="form-group col-lg-6 required">
						<label class="h4 control-label">Email</label>
						<?php  
							if (isset($_SESSION['mail'])){
								echo '<input type="email" class="form-control" name="email" value="'.$_SESSION['mail'].'" required>';
							}
							else {
								echo '<input type="email" class="form-control" name="email" placeholder="Email" required>';
							}
						?>
					</div>
				</div>
				
				<div class="form-group required">
					<label for="message" class="h4 control-label">Raisons pour lesquelles vous postulez</label>
					<textarea name="message" class="form-control" rows="5" placeholder="Ecrivez votre message" required></textarea>
				</div>
				<button type="submit" class="btn bouton float-right" name="soumettre">Soumettre</button>
			</div>
			<div class="col-lg-10 offset-1 text-danger">* Champs obligatoires</div>
		</form>
	</div>
	<?php require "footer.php" ?>
</body>
</html>