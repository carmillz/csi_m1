<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	if(empty($_SESSION['id_util']) || !isset($_SESSION['id_util'])) {
		header("Location: index.php");
		exit();
	}
?>

<body class="page bg-light">
	<!-- header -->
	<?php require "header.php" ?>

	<div class="container site-content mt-1 pt-2 bg-white rounded">
		<div class="row">
			<div class="col-lg-10 offset-1">
				<div class="row">
					<div class="col-lg-12 mb-2">
						<h2>Mon compte</h2>
					</div>
				</div>
				<?php 
					/*
					0  id
					1 mail
					2 mdp
					3 nom
					4 prenom
					5 filiere
					6 niveau
					7 admin
					8 compte actif
					9 description
					*/
					$sql= "SELECT id_utilisateur,nom,prenom FROM utilisateur, (SELECT id_util_2 FROM relation WHERE  relation.statut_rel='banni' AND relation.id_util_1=".$_SESSION['id_util']." ) AS R1 WHERE utilisateur.id_utilisateur = R1.id_util_2";
					$query=pg_query($sql);
				?>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="list-group col-lg-3 text-center" role="tablist">
								<a href="account.php" class="list-group-item list-group-item-action">Mes informations</a>
								<a href="blacklist.php" class="list-group-item list-group-item-action active">Ma liste noire</a>
							</div>
							<div class="col-lg-9 text-justify tab-content">						
								<!-- BlackList-->
								<div class="card card-outline-secondary">
									<div class="card-header">Les personnes bloquées </div>
									<div class="card-body">
									<?php  
										if (pg_num_rows($query) == 0 ){
											echo '<div class="alert alert-primary text-center" role="alert">Vous n\'avez bloqué personne pour le moment !</div>';
										} 
										else {
											while ($row = pg_fetch_row($query)) {
												echo '<a href="profil.php?util='.$row[0].'">'.$row[2].' '.$row[1].'</a><br>';
											}
										}
									?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>