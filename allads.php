<!DOCTYPE html>
<html lang="fr">
<!-- head -->
<?php 
	require "head.php";
	$_SESSION['page'] ='index'; 
?>	

<body class="page bg-light">
	<?php
	require "header.php";
	?>

	<div class="container site-content mt-1 pt-2 bg-white rounded" >
		<div class="row">
			<div class="col-lg-10 offset-1 ">
				<h2> Les annonces</h2>
			</div>
			<?php 
			$sql= "select * from annonce WHERE id_util NOT IN 
			
			(select id_utilisateur from utilisateur, 
					(select id_util_1 from relation 
					WHERE relation.id_util_2=".$_SESSION['id_util']." 
					AND relation.statut_rel='banni') AS R1 
					WHERE utilisateur.id_utilisateur = R1.id_util_1)
					AND id_util != ".$_SESSION['id_util']."
					AND date_insc_max> CURRENT_TIMESTAMP
					AND statut_an ='en_cours'
					AND (filiere_dest='".$_SESSION['filiere']."' OR filiere_dest='aucun')
					AND (niv_dest='".$_SESSION['niveau']."' OR niv_dest='tous')";
				$query=pg_query($sql);

				//notification pour prevenir l'utilisateur si l'annonce a laquelle il est inscrit début dans moins de 24 heures
				$chaine ="";
				while ($row = pg_fetch_row($query)) {
					$sql2 = "SELECT notification FROM est_inscrit WHERE id_util=".$_SESSION['id_util']." AND id_annonce=".$row[0];
					$requete= pg_query($sql2);
					$ligne=pg_fetch_row($requete);
					if ((date('Y-m-d G:i',strtotime('24 hour')) >  $row[6]) && $ligne[0]=="t") {
						$chaine= $chaine.'<li><a href="ad.php?annonce='.$row[0].'">'. $row[3].'</a></li>';
					}
				}
				if ($chaine != ""){
					echo'<div class="col-lg-10 offset-1 mt-1 mb-1 alert alert-success" role="alert"> Attention ! Vous vous êtes inscrit(e) à des annonces débutant dans moins de 24 heures
					<ul>'.$chaine.'</ul></div>';
				}
				$query=pg_query($sql);
				$_SESSION['requete']=$query;
				require 'anad.php';
			?>
		</div>
	</div>
	<!--footer-->
	<?php require "footer.php" ?>
</body>
</html>